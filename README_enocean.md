ENOCEAN   
:warning: /!\--- WIP ---/!\  :warning:   
*   EnOcean - Découverte
    *   **1 - Généralités**
    *   [a - Origines](#orig)
    *   [b - Récolte d'énergie](#recolteEnergie)
    *   [c - Vue d'ensemble des spécifications](#protocoles)
    
    *   **2 - Protocoles**
    *   [a - Les EEP](#EEP)
    *   [b - L'ERP](#ERP)
    *   [c - L'ESP](#ESP)
    
![](assets/img/entete.png)

![](assets/img/logoEnOcean.png)

Généralités
===========

**origines**

La creation d’EnOcean date de 2001. Cette entreprise a ete essaimée par Siemens AG. La petite PME bénéficie donc du soutien matériel et financier de ce grand groupe international d’origine allemande. Grâce à l’élaboration d’équipements sans-fil et sans-pile basés sur des convertisseurs d’énérgie miniatures et des circuits electroniques performants, EnOcean est très appréciée pour répondre à la problématique de consommation d'énergie des réseaux de capteurs. Aujourd’hui, ces équipements sont choisis dans le monde entier par plus de 100 fabricants dans le secteur du BTP et l’industrie. La société, dont le siège social est basé à Oberhaching près de Munich, emploie actuellement environ 60 personnes en Allemagne et aux Etats-Unis.

Depuis le mois d’avril 2012, la technologie de transmission radio est normalisée sous la référence ISO/IEC 14543-3-10. La norme s’intitule « Technologies de l’information - Architecture des systemes électroniques domestiques (HES) - Partie 3-10 : Protocole de paquets de donnees courts sans fil (WSP) optimisés pour la collecte d’énergie - Architecture et protocoles de couche inférieure ».

![](assets/img/normeISO_enocean.jpg)

**recolte d'énergie**

La technologie découle d’un constat simple: l’appui sur un bouton, la variation de température ... génèrent suffisamment d’énergie pour transmettre des signaux sans fil. La condition étant de réussir a la recolter et de l'utiliser de manière optimale.Par conséquent, au lieu de piles ou de batteries, EnOcean utilise des convertisseurs d’énergie miniaturisés pour alimenter ses modules, visibles ci-dessous : des convertisseurs de mouvement (à gauche), d’énergie lumineuse (au centre) et thermique (à, droite).

![](assets/img/haversters.jpg)

Sources d'énergies utilisées par la tehnologie EnOcean

Ces convertisseurs équippent les différents capteurs et actionneurs EnOcean disponible. On peut citer par exemple:

*   Les interrupteurs et poignées de fenêtres qui utilisent le convertisseur mécanique (piezzo).
*   Les capteurs de température ou contact de détection d'ouverture qui utilisent la cellule photovoltaïque.
*   Les vannes thermostatiques qui utilisent le convertisseur thermique (effet Peltier).

**tour d'horizon des Protocoles EnOcean**

![](assets/img/protocoles.jpg)

Protocoles Radio et Série et Profils d'équipement Enocean

Afin de réduire la consommation d’énergie et d'assurer l'interopérabilité nécessaire aux différents equipements EnOcean, la société a mis en place deux protocoles de communication :

**Le Protocole Serie EnOcean 3 (ESP3):**

Ces protocoles sont conçus pour transférer des informations par l’intermédiaire de l’interface UART. Vous trouverez les spécifications détaillées de ce protocole dans le document fourni par EnOcean et disponible ici : [Spécifications ESP](assets/ressources/ESP3.pdf).

**Le Protocole Radio EnOcean (ERP):**

\-Le Protocole Radio EnOcean est optimisé pour transmettre des informations en consommant très peu d'énergie via liaison radio 868 MHz. Encore une fois, EnOcean fourni plus de détails sur ce protocole, disponible ici : [Spécifications ERP](assets/ressources/ERP2.pdf).

**Les Profils d'équipements EnOcean (EEP):**

Enfin, chaque équipement EnOcean possède son propre identifiant (4 octets) et son "profil d'équipement", qui permet de définir le contenu des trames qu'il envoie. Le catalogue contenant ces profils est disponible en telechargement au format PDF via [ce lien.](assets/ressources/EEP_263.pdf)

Ainsi, le "fond" du message est décrit par les EEP alors que la "forme" est décrite par l'ERP (radio) ou l'ESP (série). La suite de ce TP aborde alors ces trois points.

Les Protocoles
==============

**EnOcean equipment profile**

Un télégramme EnOcean est composé de différents champs. Ceux-ci seront détaillés plus loin. Nous nous intéresserons ici à la partie du message contenant les données de l'équipement (qu'il s'agisse d'un télégrame à envoyer à un actionneur, ou transmis par un capteur).

Les EEP, pour "Enocean Equipment Profile" dressent une carte complète des messages qui sont transmis. En effet, chaque equipement compatible EnOcean est forcément associé à son profil. La lecture de celui-ci nous donne alors plus d'informations:

De manière globale, les EEP sont représentés par trois octets comme suit : XX-YY-ZZ

**XX** = RORG = Radio ORGanization. Définit le type de télégramme, dont quelques exemples sont disponibles dans le tableau ci-dessous.

**YY** = FUNC = Function. Définit la fonction principale de l'équipement (capteur de température, relai commandé ...)

**ZZ** = TYPE. Définit plus précisément la fonction associée à l'équipement. Ainsi, un capteur de température peut possèder différents types en fonction de sa plage de mesure : \[0;-40°\] ou \[-10; + 30°\] etc... .

Une fois ces trois informations connues, il est possible de déterminer avec précision les télégrammes qui seront envoyés par l'équipement.

Il existe différents types de télégrammes (donc différentes RORG), dont les prncipaux sont:
|RORG (hexadécimal)|Description|Exemple|
|--- |--- |--- |
|F6|RPS (Repeated Switch Communication)|Boutons poussoirs, télécommande|
|D5|1BS (1 Byte Communication)|Contact d'ouverture|
|A5|4BS (4 Bytes Communication)|Capteur de température, de mouvement|
|D2|VLD (Variable Length Data)|Prise commandée, vanne thermostatique|


**enocean radio protocol**

Le Protocole Radio Enocean décrit la forme et le contenu des messages radio échangés dans un réseau de capteurs EnOcean.

Celui-ci couvre les trois premières couches du modèle OSI :

![](assets/img/ERP_layers.png)

Vous pouvez consulter la globalité de ce document à l'adresse suivante : [Spécifications ERP](assets/ressources/ERP2.pdf). Voici les points clés à retenir:

*   **La communication EnOcean se fait sur les fréquences libres : 868 MHz en Europe.**
*   **La modulation utilisée est de type FSK (Frequence Shift Keying) avec un codage type NRZ (Non Return to Zero).**
*   **Ce protocole permet d'atteindre un débit de 125 kbps.**
*   **Afin d'éviter toute collision, ce protocole utilise la technique du LBT (Listen Before Talk).**

La spécification nous apprend alors qu'un télégramme radio EnOcean est coposé de la manière suivante:

![](assets/img/ERP_fields.png)

|Champ|Description|Taille|
|--- |--- |--- |
|Length|Taille en octets du télégramme|8 bits|
|Header|Informations sur le télégramme : RORG, Contient-il un ID destinataire? ...|8 Bits|
|Ext. XXX|Si le télégramme en contient effectivement un, l'EXT_Header permet de spécifier le nombre de repétition du télégramme par une passerelle ou encore la longueur des OPT_DATA. L'EXT_TelType permet quant à lui de spécifier le type de télégramme particuliers, que nous n'utiliserons pas dans ce TP (SmartAck ...).|2*8 Bits|
|Orig. ID|ID unique de l'émetteur|32 Bits|
|Dest. ID|ID unique du destinataire (broadcast : Ox FF FF FF FF)|32 Bits|
|Data_DL|Données telles que décrites dans l'ESP3|X Bits|
|Opt. Data|Données optionnelles telles ue décrites dans l'ESP3|Définie dans l'EXT_Header|
|CRC|Champ qui permet le controle de l'integrité des données, appliqué sur le contenu du Data_DL. Le calcul est détaillé dans la spécification.|8 Bits|


**enocean serial protocol**

Le Protocole Radio Enocean décrit les spécifications des télégrammes sur la couche radio 868MHz. Pour arriver à ce stade, le télégramme a été généré par un composant fourni par EnOcean : le TCM (TransCeiver Module). Ce module (dont le contenu sera détaillé dans un futur TP) permet par exemple de créer une passerelle UART/Radio. Les spécifications de la commmunication UART sont alors disponibles ici : [Spécifications ESP](assets/ressources/ESP3.pdf).

![](assets/img/ESP_GView.png)

La spécification nous permet alors de déterminer le contenu d'un packet ESP3 :

![](assets/img/ESP_PackStrctureLength.png)

Il est alors possible de déterminer avec précision le contenu d'un paquet type ESP3:

|Champ|Description|Taille|
|--- |--- |--- |
|Sync|Octet de synchronisation ( 0x 55 soit 0b 01010101)|8 bits|
|Data Length|Longueur du champ DATA|16 Bits|
|Opt Length|Longueur du champ OptData|8 Bits|
|Packet Type|Type de paquet. Il en existe plusieurs mais nous n'utiliserons que le type "Radio Message" qui est la transcription d'un paquet Radio EnOcean sur l'UART.|8 Bits|
|CRC8H|CRC calculé à partir des 4 octets précédents|8 Bits|
|Data|Données telles que décrites dans l'ESP3 (dépend du type de paquet).|X Bits|
|Opt. Data|Données optionnelles telles que décrites dans l'ESP3|X Bits|
|CRC8D|CRC calculé à partir des X octets précédents|8 Bits|




Bien qu'il existe de nombreux types de paquets (par exemple pour communiquer directement avec le TCM afin de le paramétrer), nous n'utiliserons que le type 0x01. Celui-ci est la transcription directe du télégramme radio sous cette forme :

![](assets/img/ESP_Type01.png)  

En pratique (à mettre à jour)
================

**exemple d'un capteur de temperature**

![](assets/img/NodOn.jpg)![](assets/img/CapTemp.jpg)

![](assets/img/CapTempSpec.jpg)

Le profil d'équipement de ce capteur de température est le suivant : **A5-02-05.**

La lecture du catalogue d'EEP nous donne :

**A5** = 4BS telegram

La donnée sera donc codée sur 4 octets

**02** = Temperature sensor

**05** = Temperature sensor range 0°C to +40°C

|||
|--- |--- |
|Hexadécimal|01:82:22:80|
|Binaire|0000 0001 1000 0010 0010 0100 1000 0000|
|Sender ID|Identifiant du capteur EnOcean : 01822280|

|--- |--- |
|Hexadécimal|00|
|Binaire|0000 0000|
|Signification|0 répétition du télégramme par une passerelle|
